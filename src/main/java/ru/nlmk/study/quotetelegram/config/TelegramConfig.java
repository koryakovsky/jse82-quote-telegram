package ru.nlmk.study.quotetelegram.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import ru.nlmk.study.quotetelegram.telegram.QuotesBot;

import javax.annotation.PostConstruct;

@Configuration
public class TelegramConfig {

    static {
        ApiContextInitializer.init();
    }

    @PostConstruct
    public void initApi() {
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(createQuotesBot());
        } catch (TelegramApiRequestException e) {
            throw new RuntimeException(e);
        }
    }

    @Bean
    public QuotesBot createQuotesBot() {
        return new QuotesBot();
    }
}
