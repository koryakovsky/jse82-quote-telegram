package ru.nlmk.study.quotetelegram.telegram;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.nlmk.study.quotetelegram.model.Quote;

import java.util.HashSet;
import java.util.Set;

@Slf4j
@Component
public class QuotesBot extends TelegramLongPollingBot {

    @Value("${telegram.bot.token}")
    private String token;

    @Value("${telegram.bot.name}")
    private String name;

    private static final Set<Long> ids = new HashSet<>();

    @Override
    public void onUpdateReceived(Update update) {
        ids.add(update.getMessage().getChatId());

        SendMessage msg = new SendMessage();
        msg.enableMarkdown(true);
        msg.setChatId(update.getMessage().getChatId());
        msg.setText("You are subscribed");

        try {
            execute(msg);
        } catch (Exception e) {
            log.error("something gone wrong");
        }
    }

    public void sendQuotes(Quote quote) {
        SendMessage msg = new SendMessage();
        msg.enableMarkdown(true);
        msg.setText(quote.getText());

        try {
            for (Long id : ids) {
                msg.setChatId(id);
                execute(msg);
                log.info("sent quote to user: {}", id);
            }
        } catch (Exception e) {
            log.error("something gone wrong");
        }
    }

    @Override
    public String getBotUsername() {
        return name;
    }

    @Override
    public String getBotToken() {
        return token;
    }
}
