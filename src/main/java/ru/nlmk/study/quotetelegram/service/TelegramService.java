package ru.nlmk.study.quotetelegram.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nlmk.study.quotetelegram.model.Quote;
import ru.nlmk.study.quotetelegram.telegram.QuotesBot;

@Service
public class TelegramService {

    private final QuotesBot quotesBot;

    @Autowired
    public TelegramService(QuotesBot quotesBot) {
        this.quotesBot = quotesBot;
    }

    public void processQuote(Quote quote){
        quotesBot.sendQuotes(quote);
    }
}
