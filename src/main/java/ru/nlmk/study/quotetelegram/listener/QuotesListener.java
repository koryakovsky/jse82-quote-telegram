package ru.nlmk.study.quotetelegram.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.nlmk.study.quotetelegram.model.Quote;
import ru.nlmk.study.quotetelegram.service.TelegramService;

@Slf4j
@Component
public class QuotesListener {

    private final TelegramService telegramService;

    @Autowired
    public QuotesListener(TelegramService telegramService) {
        this.telegramService = telegramService;
    }

    @RabbitListener(queues = "${spring.rabbitmq.queue}")
    public void consumeMessage(Quote quote) {
        log.info("received quote: {}", quote.getText());

        telegramService.processQuote(quote);
    }
}
