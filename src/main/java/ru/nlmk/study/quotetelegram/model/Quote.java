package ru.nlmk.study.quotetelegram.model;

import lombok.Data;

@Data
public class Quote {

    private String text;

    private String author;

    private String tag;
}
