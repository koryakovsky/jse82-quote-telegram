package ru.nlmk.study.quotetelegram;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuoteTelegramApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuoteTelegramApplication.class, args);
	}

}
